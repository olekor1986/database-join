<?php
require_once "connect.php";

$categories = [
    [
        'title' => 'Laptops'
    ],
    [
        'title' => 'Phones'
    ],
    [
        'title' => 'Watches'
    ]
];


$users = [
    [
        'name' => 'User1',
        'email' => 'user1@mail.com',
        'password' => '12345678'
    ],
    [
        'name' => 'User2',
        'email' => 'user2@mail.com',
        'password' => '12345678'
    ],
    [
        'name' => 'User3',
        'email' => 'user3@mail.com',
        'password' => '12345678'
    ],
    [
        'name' => 'User4',
        'email' => 'user4@mail.com',
        'password' => '12345678'
    ],
    [
        'name' => 'User5',
        'email' => 'user5@mail.com',
        'password' => '12345678'
    ]
];


$products = [
    [
        'title' => 'Ноутбук Lenovo IdeaPad 330-15AST',
        'price' => 7599,
        'description' => 'Lenovo Ideapad 330 — это 15.6-дюймовый высокопроизводительный ноутбук, 
        который отлично подойдет для работы и развлечений. Ноутбук оснащен производительным 
        процессором, высокоскоростным модулем Wi-Fi и акустической системой отличного качества.',
        'category_id' => 1
    ],
    [
        'title' => 'Ноутбук MSI PS42 Modern 8MO',
        'price' => 18000,
        'description' => 'MSI PS42 Modern 8MO это 14-дюймовый ноутбук. Используя процессор 8-го 
        поколения Intel и сверхбыстрый твердотельный накопитель, ноутбук может похвастаться 
        высокой вычислительной производительностью и моментальной загрузкой операционной системы.',
        'category_id' => 1
    ],
    [
        'title' => 'Ноутбук Asus X509FL-BQ053',
        'price' => 16777,
        'description' => 'Ноутбуки ASUS серии X могут похвастать великолепными мультимедийными 
        возможностями. Эксклюзивная аудиотехнология SonicMaster, а также фирменные технологии 
        ASUS Splendid и ASUS Tru2Life Video, обеспечивают беспрецедентное для мобильных компьютеров 
        качество звучания и отличное изображение.',
        'category_id' => 1
    ],
    [
        'title' => 'Ноутбук Dell Inspiron 3582',
        'price' => 9599,
        'description' => 'Универсальный 15.6-дюймовый ноутбук по доступной цене Dell Inspiron 3582, 
        оснащенный развлекательными возможностями, которые обеспечивают великолепное качество 
        воспроизведения мультимедийных материалов.',
        'category_id' => 1
    ],
    [
        'title' => 'Смартфон Samsung Galaxy M30s 4/64GB',
        'price' => 7299,
        'description' => 'Встречайте – супермощный смартфон Samsung Galaxy M30s, который способен 
        перевернуть ваше представление об автономности современных телефонов. С ним можно наслаждаться 
        великолепным изображением на экране, качеством получаемых фотоснимков и видеороликов, а также быстрой 
        работой всех приложений. Его оптимальное соотношение габаритов, размера дисплея и мощности сделает 
        его вашим любимцем на долгие годы. И теперь у вас будет универсальный девайс для игр, работы и учебы.',
        'category_id' => 2
    ],
    [
        'title' => 'Apple iPhone XR 64Gb Black',
        'price' => 19000,
        'description' => 'Лучший ЖК-экран Liquid Retina, обновленный Face ID и новейший процессор. Это лишь малая часть 
        появившихся улучшений в новом iPhone Xr. Работа всех его функций основывается на интеллектуальных возможностях 
        ультрасовременного чипа и даже камера может по-настоящему удивить вас. Новый iPhone идеально вписывается в габариты 
        предыдущих устройств компании, при этом, наделяя вас огромным экраном с прекрасными возможностями.',
        'category_id' => 2
    ],
    [
        'title' => 'Смартфон Huawei P Smart Z',
        'price' => 6500,
        'description' => 'Смартфон HUAWEI P smart Z с безрамочным FullView-дисплеем, станет для вас одним из самых 
        ярких событий в мире техники благодаря его особому стилю исполнения и ряду первоклассных конструктивных 
        особенностей. Прячущаяся в корпус смартфона селфи-камера, неповторимые расцветки, высочайшая надежность всех 
        механизмов, а также первоклассные возможности искусственного интеллекта. Отличные камеры в сочетании с ярким 
        дисплеем и мощным аккумулятором помогут развлечься и поработать, а для всего остального есть NFC.',
        'category_id' => 2
    ],
    [
        'title' => 'Смарт-часы Samsung Galaxy Watch 46mm Silver',
        'price' => 9500,
        'description' => 'Ведите более насыщенную, активную и умную жизнь со смарт-часами Samsung Galaxy Watch, 
        которые позволяют оставаться на связи и не брать с собой телефон*. Хорошо отдыхайте и продуктивно работайте 
        со встроенным отслеживанием работоспособности. С данным устройством все, что вам нужно, — у вас на запястье!',
        'category_id' => 3
    ],
    [
        'title' => 'Смарт-часы Huawei Watch GT Stainless Steel',
        'price' => 5899,
        'description' => 'Благодаря керамическому безелю, корпусу из нержавеющей стали и алмазоподобному покрытию 
        (технология DLC) часы устойчивы к повреждениям. Корпус Huawei Watch GT оснащен двумя заводными головками. 
        Цветной экран AMOLED с диагональю 1.39 дюйма, высотой 10.6 мм и разрешением 454 x 454 точно и ярко передает 
        изображение.',
        'category_id' => 3
    ],
    [
        'title' => 'Смарт-часы Amazfit Bip',
        'price' => 1763,
        'description' => 'Часы Amazfit Bip подарят вам фантастическую автономность, водонепроницаемость, наличие датчиков 
        сердечного ритма и геопозиционирования, а также предоставят подробную статистику фитнес-активности. Управление жестами 
        и синхронизация со смартофоном сделают процесс управления часами легким и приятным. ',
        'category_id' => 3
    ]
];

$orders = [
    [
        'user_id' => 1,
        'phone' => '077-777-11-11',
        'comment' => 'Hello, im User1, it is my comment!'
    ],
    [
        'user_id' => 2,
        'phone' => '077-777-22-22',
        'comment' => 'Hello, im User2, it is my comment!'
    ],
    [
        'user_id' => 3,
        'phone' => '077-777-33-33',
        'comment' => 'Hello, im User3, it is my comment!'
    ],
    [
        'user_id' => 4,
        'phone' => '077-777-44-44',
        'comment' => 'Hello, im User4, it is my comment!'
    ],
    [
        'user_id' => 5,
        'phone' => '077-777-55-55',
        'comment' => 'Hello, im User5, it is my comment!'
    ]
];

$order_product = [
    [
        'order_id' => 1,
        'product_id' => 1,
        'amount' => 2
    ],
    [
        'order_id' => 1,
        'product_id' => 2,
        'amount' => 2
    ],
    [
        'order_id' => 1,
        'product_id' => 3,
        'amount' => 2
    ],
    [
        'order_id' => 2,
        'product_id' => 1,
        'amount' => 5
    ],
    [
        'order_id' => 2,
        'product_id' => 4,
        'amount' => 5
    ],
    [
        'order_id' => 2,
        'product_id' => 6,
        'amount' => 5
    ],
    [
        'order_id' => 3,
        'product_id' => 2,
        'amount' => 1
    ],
    [
        'order_id' => 3,
        'product_id' => 5,
        'amount' => 1
    ],
    [
        'order_id' => 3,
        'product_id' => 7,
        'amount' => 1
    ],
    [
        'order_id' => 4,
        'product_id' => 3,
        'amount' => 3
    ],
    [
        'order_id' => 4,
        'product_id' => 7,
        'amount' => 3
    ],
    [
        'order_id' => 4,
        'product_id' => 1,
        'amount' => 3
    ],
    [
        'order_id' => 5,
        'product_id' => 9,
        'amount' => 4
    ],
    [
        'order_id' => 5,
        'product_id' => 8,
        'amount' => 4
    ],
    [
        'order_id' => 5,
        'product_id' => 7,
        'amount' => 4
    ]
];

try {
    foreach ($categories as $value) {
        $sql = "INSERT INTO categories SET
                title = :title";
        $seederObject = $db->prepare($sql);
        $seederObject->bindValue(':title', $value['title']);
        $seederObject->execute();
    }
} catch (Exception $e) {
    $message = "Ошибка наполнения таблицы categories: " . $e->getMessage();
    die($message);
}

try {
    foreach ($users as $value) {
        $sql = "INSERT INTO users SET
                name = :name,
                email = :email,
                password = :password";
        $seederObject = $db->prepare($sql);
        $seederObject->bindValue(':name', $value['name']);
        $seederObject->bindValue(':email', $value['email']);
        $seederObject->bindValue(':password', md5($value['password']));
        $seederObject->execute();
    }
} catch (Exception $e) {
    $message = "Ошибка наполнения таблицы users: " . $e->getMessage();
    die($message);
}

try {
    foreach ($products as $value) {
        $sql = "INSERT INTO products SET
                title = :title,
                price = :price,
                description = :description,
                category_id = :category_id";
        $seederObject = $db->prepare($sql);
        $seederObject->bindValue(':title', $value['title']);
        $seederObject->bindValue(':price', $value['price']);
        $seederObject->bindValue(':description', $value['description']);
        $seederObject->bindValue(':category_id', $value['category_id']);
        $seederObject->execute();
    }
} catch (Exception $e) {
    $message = "Ошибка наполнения таблицы products: " . $e->getMessage();
    die($message);
}


try {
    foreach ($orders as $value) {
        $sql = "INSERT INTO orders SET
                user_id = :user_id,
                phone = :phone,
                comment = :comment";
        $seederObject = $db->prepare($sql);
        $seederObject->bindValue(':user_id', $value['user_id']);
        $seederObject->bindValue(':phone', $value['phone']);
        $seederObject->bindValue(':comment', $value['comment']);
        $seederObject->execute();
    }
} catch (Exception $e) {
    $message = "Ошибка наполнения таблицы orders: " . $e->getMessage();
    die($message);
}

try {
    foreach ($order_product as $value) {
        $sql = "INSERT INTO order_product SET
                order_id = :order_id,
                product_id = :product_id,
                amount = :amount";
        $seederObject = $db->prepare($sql);
        $seederObject->bindValue(':order_id', $value['order_id']);
        $seederObject->bindValue(':product_id', $value['product_id']);
        $seederObject->bindValue(':amount', $value['amount']);
        $seederObject->execute();
    }
} catch (Exception $e) {
    $message = "Ошибка наполнения таблицы order_product: " . $e->getMessage();
    die($message);
}

header("Location:index.php");
?>