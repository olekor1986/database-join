<?php
require_once "connect.php";

try {
    $sql = "CREATE TABLE categories (
        id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
        title VARCHAR (255) NOT NULL
        ) DEFAULT CHARACTER SET utf8 ENGINE=InnoDB" ;
    $db->exec($sql);
} catch (Exception $e) {
    $message = 'Не удается создать таблицу categories: ' . $e->getMessage();
    die($message);
}

try {
    $sql = "CREATE TABLE users (
        id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
        name VARCHAR (255) NOT NULL,
        email VARCHAR (255) NOT NULL,
        password VARCHAR (255) NOT NULL
        ) DEFAULT CHARACTER SET utf8 ENGINE=InnoDB" ;
    $db->exec($sql);
} catch (Exception $e) {
    $message = 'Не удается создать таблицу users: ' . $e->getMessage();
    die($message);
}

try {
    $sql = "CREATE TABLE products (
        id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
        title VARCHAR (255) NOT NULL,
        price FLOAT NOT NULL,
        description VARCHAR (1000),
        category_id INT
        ) DEFAULT CHARACTER SET utf8 ENGINE=InnoDB" ;
    $db->exec($sql);
} catch (Exception $e) {
    $message = 'Не удается создать таблицу products: ' . $e->getMessage();
    die($message);
}

try {
    $sql = "CREATE TABLE orders (
        id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
        user_id INT NOT NULL,
        phone VARCHAR (255),
        comment VARCHAR (1000)
        ) DEFAULT CHARACTER SET utf8 ENGINE=InnoDB" ;
    $db->exec($sql);
} catch (Exception $e) {
    $message = 'Не удается создать таблицу orders: ' . $e->getMessage();
    die($message);
}

try {
    $sql = "CREATE TABLE order_product (
        id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
        order_id INT NOT NULL,
        product_id INT NOT NULL,
        amount INT NOT NULL
        ) DEFAULT CHARACTER SET utf8 ENGINE=InnoDB" ;
    $db->exec($sql);
} catch (Exception $e) {
    $message = 'Не удается создать таблицу order_product: ' . $e->getMessage();
    die($message);
}


header("Location:index.php");
?>